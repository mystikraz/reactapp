import React from 'react';
import logo from './logo.svg';
import './App.css';
import SubmitForm from './SubmitForm';
const movieData = [
  {
    title: 'Avengers: Infinity War',
    year: '2018',
    description: 'Iron Man, Thor, the Hulk and the rest of the Avengers unite to battle their most powerful enemy yet -- the evil Thanos. On a mission to collect all six Infinity Stones, Thanos plans to use the artifacts to inflict his twisted will on reality.',
    imageURL: 'https://via.placeholder.com/362x200',
  },
  {
    title: 'Bohemian Rhapsody',
    year: '2018',
    description: 'Bohemian Rhapsody is a foot-stomping celebration of Queen, their music and their extraordinary lead singer Freddie Mercury. Freddie defied stereotypes and shattered convention to become one of the most beloved entertainers on the planet.',
    imageURL: 'https://via.placeholder.com/362x200',
  },
  {
    title: 'The Incredibles 2',
    year: '2018',
    description: 'Everyone’s favorite family of superheroes is back in “Incredibles 2” – but this time Helen is in the spotlight, leaving Bob at home with Violet and Dash to navigate the day-to-day heroics of “normal” life.',
    imageURL: 'https://via.placeholder.com/362x200',
  },
];
const MovieCard = ({ title, year, description, imageURL }) => (
  <div className="card">
    <img className="card-img-top" src={imageURL[0].url} alt="Movie poster" />
    <div className="card-body">
      <h5 className="card-title">{title}</h5>
      <p className="card-text">{description}</p>
      <p className="card-text">
        <small className="text-muted">{year}</small>
      </p>
    </div>
  </div>
);

class App extends React.Component {
  // state = {
  //   tasks: ['task 1', 'task 2', 'task 3']
  // };
  // render() {
  //   return (
  //     <div className='wrapper'>
  //       <div className='card frame'>
  //         <Header numTodos={this.state.tasks.length} />
  //         <TodoList tasks={this.state.tasks} onDelete={this.handleDelete} />
  //         <SubmitForm onFormSubmit={this.handleSubmit} />
  //       </div>
  //     </div>
  //   );
  // }
  // handleDelete = (index) => {
  //   const newArr = [...this.state.tasks];
  //   newArr.splice(index, 1);
  //   this.setState({ tasks: newArr });
  // }
  // handleSubmit = task => {
  //   this.setState({ tasks: [...this.state.tasks, task] });
  // }
  // componentDidMount(){

  // }
  constructor(props) {
    super(props);
    this.state = {
      movies: [],
    };
  }
  componentDidMount(){
    fetch('https://api.airtable.com/v0/app7pOXej7S4l8Be2/favourites?api_key=keychnQg7E2u57FeM')
    .then((resp)=>resp.json())
    .then(data=>{
      this.setState({movies:data.records});
    }).catch(err=>{
      console.log(err);
    })
  }
  render() {
    return (
      <div className="container mt-5">
        <div className="row">
          <div className="col">
            <div className="card-deck">
              {/* {movieData.map(movie => <MovieCard {...movie} />)} */}
              {this.state.movies.map(movie => <MovieCard {...movie.fields} />)}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const Header = (props) => {
  return (<div className='card-header'>
    <h1 className='card-header-title header'>
      You have {props.numTodos} Todos
    </h1>
  </div>)
}

const TodoList = (props) => {
  const todos = props.tasks.map((todo, index) => {
    return <Todo content={todo} key={index} id={index} onDelete={props.onDelete} />
  })
  return (
    <div className='list-wrapper'>
      {todos}
    </div>
  );
}

const Todo = (props) => {
  return (<div className='list-item'>
    {props.content}
    <button className='delete is-pulled-right' onClick={() => { props.onDelete(props.id) }}>delete</button>
  </div>);
}

// class SubmitForm extends React.Component {
//   state = { term: '' };
//   handleSubmit = (e) => {
//     e.preventDefault();
//     if (this.state.term === '') return;
//     this.props.onFormSubmit(this.state.term);
//     this.setState({ term: '' });
//   }
//   render() {
//     return (
//       <form onSubmit={this.handleSubmit}>
//         <input type='text' className='input' placeholder='Enter Item' value={this.state.term}
//           onChange={(e) => this.setState({ term: e.target.value })} />
//         <button className='button'>Submit</button>
//       </form>
//     )
//   }

// }

export default App;
